**libmcad** allows you to use different atomic multicast algorithms, by implementing and adaptor for each different implemention.

**libmcad_dep** contains the adaptor implementation for Ridge atomic multicast and Multi-Ring-Paxos
(this repository is a parent for repositories: libmcad, Ridge, sense, netwrapper and Multi-Ring-Paxos).

The fastest way to test the atomic multicast algorithm (using ridge implementation) is:

1) git clone --recursive https://bitbucket.org/kdubezerra/libmcad_dep.git
2) cd libmcad_dep
3) mvn package -DskipTests -Dmaven.javadoc.skip=true
4) cd exampleLink
5) at a terminal 1, run ./deployMcast.py
6) at a terminal 2, run ./deployReceiver.py 9 1
7) at a terminal 3, run ./deployReceiver.py 10 2
8) at a terminal 4, run ./deploySender.py
